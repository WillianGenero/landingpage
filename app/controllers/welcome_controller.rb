class WelcomeController < ApplicationController
  def index
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(contact_params)
    respond_to do |format|
      if @contact.save
        ContactMailer.with(user: @contact).welcome_email.deliver_later
        format.js
      else
        format.js
      end
    end
  end

  private
    def contact_params
      params.require(:contact).permit(:name, :email, :phone, :message)
    end
end
