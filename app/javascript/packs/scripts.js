document.addEventListener('turbolinks:load', () => {
  const openMenu = document.getElementById("open_menu_mobile")
  const closeMenu = document.getElementById("close_menu_mobile")
  const menu = document.getElementsByClassName("menu_mobile_options")[0]

  openMenu.addEventListener('click', (event) => {
    menu.style.display = 'flex'
  })

  closeMenu.addEventListener('click', (event) => {
    menu.style.display = 'none'
  })
})

document.addEventListener('turbolinks:load', () => {
  var slideIndex = 0
  showSlides()
  function showSlides() {
    const slides = document.getElementsByClassName("slide")

    for (var i = 0 ; i < slides.length ; i++) {
      slides[i].style.display = "none"
    }

    slideIndex++
    if (slideIndex > slides.length) {
      slideIndex = 1
    }

    slides[slideIndex-1].style.display = "block"
    setTimeout(showSlides, 3000)
  }
})
