class ContactMailer < ApplicationMailer
  default from: 'willian@twoweb.com.br'

  def welcome_email
    @user = params[:user]
    mail(to: @user.email, subject: 'Testanto e-mail')
  end
end
