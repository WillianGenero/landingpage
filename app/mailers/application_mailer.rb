class ApplicationMailer < ActionMailer::Base
  default from: 'willian@twoweb.com.br'
  layout 'mailer'
end
