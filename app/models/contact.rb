class Contact < ApplicationRecord
  validates :name, presence: true
  validates :phone, presence: true

  before_save { self.email = email.downcase }
  validates :email, presence: true
end
